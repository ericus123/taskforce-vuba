import { Badge, Container, Nav, Navbar, NavDropdown } from "react-bootstrap"
import { ArrowRightSquareFill, Cart, PersonPlusFill } from "react-bootstrap-icons";
import { Link} from 'react-router-dom';
import "../App.scss";
import { useSelector } from "react-redux";

import profile_pic from "../assets/images/AMANI_Eric.jpg";


const Navigation = ( ) => {
  
      const cart = useSelector(state => state.CartReducer.cart);
      const isLoggedIn = useSelector(state => state.LoginReducer.isLoggedIn);
    return <Navbar fixed="top" collapseOnSelect expand="lg" bg="dark" variant="dark">
  <Container>
  <Navbar.Brand><Link className="text-decoration-none" to={"/"} style={{color:"white"}}>FOOD WEB</Link> </Navbar.Brand>
  <Navbar.Toggle aria-controls="responsive-navbar-nav" />
  <Navbar.Collapse id="responsive-navbar-nav">
    <Nav className="me-auto">
     <Link to={"/food"} className="text-decoration-none" style={{color:"white"}}> All Food</Link>
      {/* <Nav.Link href="#pricing">Pricing</Nav.Link> */}
      {/* <NavDropdown title="Dropdown" id="collasible-nav-dropdown">
        <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
        <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
        <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
        <NavDropdown.Divider />
        <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
      </NavDropdown> */}
    </Nav>
    <Nav>
     {
       !isLoggedIn ? (<> <Link to="/login" className="text-decoration-none" style={{color:"white"}}> <span><ArrowRightSquareFill/> Sign In</span></Link>&nbsp;
      &nbsp;
      <Link eventKey={2} to="/signup" className="text-decoration-none" style={{color:"white"}}>
        <span className="signup-btn"><PersonPlusFill/> SignUp</span>
      </Link></>) : <> 
      <Link style={{margin:"auto",paddingLeft:"15px"}} to={"/food/in-cart"} ><Cart  className="shop-cart"/><Badge>{cart.length}</Badge></Link>
      
      <NavDropdown title={<img className="prof-img" src={profile_pic} alt="AMANI Eric"/>} id="collasible-nav-dropdown">
        <NavDropdown.Item> <Link to={"/food/in-cart"} style={{textDecoration:"none"}}>In Cart</Link></NavDropdown.Item>
        <NavDropdown.Item ><Link to={"/food/orders"} style={{textDecoration:"none"}}>My Orders</Link></NavDropdown.Item>
        <NavDropdown.Divider />
        <NavDropdown.Item href="/">Logout</NavDropdown.Item>
      </NavDropdown>
      </>
     }
    </Nav>
  </Navbar.Collapse>
  </Container>
</Navbar> 
};

export default Navigation;