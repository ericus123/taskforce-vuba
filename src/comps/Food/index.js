import { Button, Card, Col, Form, FormControl, Row } from "react-bootstrap";
import food from "./food.json";

import "./styles.scss";
import { Cart, Dot, Star } from "react-bootstrap-icons";
import salad from "../../assets/images/salad.jpg";
import fried_chicken from "../../assets/images/fried_chicken.jpg";
import burger from "../../assets/images/burger.jpg";
import { simpleNotification } from "../../utils/Notification";
import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { cartRequest } from "../../redux/actions";


const AllFood = () => {
 const isLoggedIn = useSelector(state => state.LoginReducer.isLoggedIn);
 const history = useHistory();
 if(!isLoggedIn){
   history.push("/login");
 }
  const dispatch = useDispatch();
  const [filtered,setFilter] = useState(food);
  useEffect(() => {

  }, [filtered])
const handleClick = (item) => {
 dispatch(cartRequest(item));
}
    return (
<div className="container food-page">
  
<div className="cat-choice">
  <Form className="d-flex p-3">
      <FormControl
        type="search"
        placeholder="I'm looking for.."
        className="mr-2 p-3"
        aria-label="Search"
        disabled
      />
       <FormControl
       style={{height:"9vh"}}
       as="select"
       onChange={(e) => { setFilter(food.filter((item) => item.category === e.target.value))}}
      >  <option disabled className="text-center">Select Your Choice</option>
          <option>Burgers</option>
          <option>Meat</option>
          <option>Vegetables and Legumes</option>
      </FormControl>
    </Form>
    </div>
    <Row className="food-row mt-5">
{filtered.map((item) => <Col>
<Card  style={{ width: '18rem' }}>
  <Card.Img variant="top" src={item.image === "../../assets/images/fried_chicken.jpg" ? fried_chicken : item.image === "../../assets/images/salad.jpg" ? salad : item.image === "../../assets/images/burger.jpg" ? burger : ""}/>
  <Card.Body>
    <Card.Title>{item.name}</Card.Title>
    <Card.Text>
      Some quick example text to build on the card title and make up the bulk of
      the card's content.
    </Card.Text>
    <Row><Col>{item.restorant}</Col><Col style={{color: item.restorant_status === "open" ? "green" : "red"}}>{item.restorant_status}</Col></Row>
  <span>{[1,2,3,4].map(() => <Star style={{color:"orange"}}/>)}</span>
    <h1 className="item-price"><span className="bef-price"><strike>20000RWF</strike></span> {item.price}</h1>
    <Button onClick={() => {simpleNotification("Item Added In Cart", "SUCCESS"); handleClick(item)}} className="cart-add-btn">Add to cart</Button>
  </Card.Body>
</Card>
<br/>
</Col>)}
        </Row>
</div>
    );

};
export default AllFood;