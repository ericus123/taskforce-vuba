import {Row, Card, Col, Button, Modal } from "react-bootstrap";
import { ArrowLeft, Cart4, Star } from "react-bootstrap-icons";
import "./styles.scss";
import salad from "../../assets/images/salad.jpg";
import fried_chicken from "../../assets/images/fried_chicken.jpg";
import burger from "../../assets/images/burger.jpg";
import { useSelector } from "react-redux";
import { useState } from "react";
import { Link } from "react-router-dom";
import PaymentForm from "./comps/Payment";
import Address from "./comps/Address";

const FoodInCart = () => {
    const [step,setStep] = useState("details");
      const [show,setShow] = useState(false);
      const [item,setItem] = useState({});
      const handleClose = () => {
          setShow(false);
      }

      const cart = useSelector(state => state.CartReducer.cart);
return (
    <div className="container in-cart">
        <Row>
        
        {
            cart?.map((item) => 
<Col>
<Card  style={{ width: '18rem' }}>
  <Card.Img variant="top" src={item.image === "../../assets/images/fried_chicken.jpg" ? fried_chicken : item.image === "../../assets/images/salad.jpg" ? salad : item.image === "../../assets/images/burger.jpg" ? burger : ""}/>
  <Card.Body>
    <Card.Title>{item.name}</Card.Title>
    <Card.Text>
      Some quick example text to build on the card title and make up the bulk of
      the card's content.
    </Card.Text>
     <Row><Col>{item.restorant}</Col><Col style={{color: item.restorant_status === "open" ? "green" : "red"}}>{item.restorant_status}</Col></Row>
  <span>{[1,2,3,4].map(() => <Star style={{color:"orange"}}/>)}</span>
    <h1 className="item-price"><span className="bef-price"><strike>20000RWF</strike></span> {item.price}</h1>
  </Card.Body>
      <Button className="cart-add-btn" onClick={() => {setShow(true); setItem(item); setStep("details")}}>Proceed</Button>
</Card>
<br/>
</Col>
            )
        }
        </Row>
       {!cart.length ?  <h1 className="in-cart-tit"><Cart4 size={50}/> Cart is empty</h1> : null}

       
      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
        size="lg"
      >
        <Modal.Header>
          <Row style={{width: "100%",fontWeight:"bolder", fontSize:"20px"}}><Col>Shopping Cart</Col><Col style={{cursor:"pointer"}}><Link style={{TextDecoration:"none",color:"black"}} to={"/food"}><b><ArrowLeft/></b> Return To Shopping</Link></Col></Row>
        </Modal.Header>
        <Modal.Body>
         <Row>
             <Col>
             <Card  style={{ width: '18rem' }}>
  <Card.Img variant="top" src={item.image === "../../assets/images/fried_chicken.jpg" ? fried_chicken : item.image === "../../assets/images/salad.jpg" ? salad : item.image === "../../assets/images/burger.jpg" ? burger : ""}/>
  <Card.Body>
    <Card.Title>{item.name}</Card.Title>
    <Card.Text>
      Some quick example text to build on the card title and make up the bulk of
      the card's content.
    </Card.Text>
    <Row><Col>{item.restorant}</Col><Col style={{color: item.restorant_status === "open" ? "green" : "red"}}>{item.restorant_status}</Col></Row>
  <span>{[1,2,3,4].map(() => <Star style={{color:"orange"}}/>)}</span>
    <h1 className="item-price"><span className="bef-price"><strike>20000RWF</strike></span> {item.price}</h1>
  </Card.Body>
</Card>
<br/>
         </Col>
             <Col>
             {step === "payment" ?  <PaymentForm/> : step === "details" ? <div>
<h6  style={{textAlign:"center"}}>Fill in your details</h6>
<Address/>

             </div> : step === "finish" ? <div><h6 voca style={{textAlign:"center"}}>Confirm order details</h6></div> : null}
             
             </Col>
         </Row>
        </Modal.Body>
        <Modal.Footer>
            {step === "finish" ? <><Button className="cart-close" onClick={() => setStep("payment")}>
            Back
          </Button> <Button variant="danger" onClick={() => handleClose()}>
            Cancel
          </Button> <Button variant="success">Confirm Order</Button> </>: 
            <>
       {step === "details" ? null : <Button className="cart-close" onClick={() => setStep("details")}>
            Back
          </Button>}
          <Button variant="danger" onClick={() => handleClose()}>
            Cancel
          </Button> 
          <Button variant="success" onClick={() =>
          { 
            if(step === "payment"){
              setStep("finish"); 
            }else{
                setStep("payment");
            }
        }
            }>Next</Button>
            </>
}
        </Modal.Footer>
      </Modal>
        
        </div>
);
};
export default FoodInCart;