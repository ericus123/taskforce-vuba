import "./styles.scss";
import { Button, Carousel, Col, Form, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import { simpleNotification } from "../../utils/Notification";
import { Eye } from "react-bootstrap-icons";

const Signup = () => {

  const handleSubmit = (e) => {
    e.preventDefault();
    simpleNotification(`Verification email has been sent to ${e.target.email.value}`,"SUCCESS");
  }
return (
<div className="auth-container">
    <br/>
    <Row className="auth-row-signup pt-5">
    <Col className="auth-col-1 pt-5">
    <div className="login">
        <h1 className="login-tit">Signup</h1>
        <p className="login-slo">Create an account and get unlimited services</p>
    {/* <div className="google-signin p-2 text-center">
       <Google/> Sign in with Google
    </div>
    <br/>
    <h1 className="text-center or">Or</h1>
    <br/> */}
 
    <Form className="login-form" onSubmit={handleSubmit}>
        <Row>
            <Col>
  <Form.Group className="mb-3" controlId="formBasicEmail">
    <Form.Label>Name*</Form.Label>
    <Form.Control type="text" className="form-input" placeholder="Enter name" required />
  </Form.Group>
 
  <Form.Group className="mb-3" controlId="formBasicPassword">
    <Form.Label>Password*</Form.Label>
    <Form.Control type="password" className="form-input" placeholder="Password" required/>
  </Form.Group>
  </Col>
    <Col>
     <Form.Group className="mb-3" controlId="formBasicEmail">
    <Form.Label>Email*</Form.Label>
    <Form.Control type="email" name="email" className="form-input" placeholder="Enter email" required />
  </Form.Group>
   <Form.Group className="mb-3" controlId="formBasicPassword">
    <Form.Label>Confirm Password*</Form.Label>
    <Form.Control type="password" className="form-input" placeholder="Password" required/>
  </Form.Group>
  </Col>
  </Row>

  
  <Button variant="primary" className="login-btn" type="submit">
    Signup
  </Button>
  
  <Row><Col>Already have an account? </Col><Col><Link to="/login" className="text-decoration-none">Sign in</Link></Col></Row>
</Form>
    </div>
    </Col>
    {/* <Col className="auth-col-2 pt-5">
    <Slider/>
    </Col> */}
    </Row>
    </div>
)
};

export default Signup;