import { Carousel } from "react-bootstrap";
import salad from "../../assets/images/salad.jpg";
import burger from "../../assets/images/burger.jpg";
import fried_chicken from "../../assets/images/fried_chicken.jpg";


const Slider = () => {
return (
<Carousel fade controls={false}>
  <Carousel.Item>
    <img
      className="d-block w-100"
      src={salad}
      alt="Salad"
    />
     <Carousel.Caption>
      <h3>Salad</h3>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
    </Carousel.Caption>
  </Carousel.Item>
    <Carousel.Item>
    <img
      className="d-block w-100"
      src={burger}
      alt="Burger"
    />
     <Carousel.Caption>
      <h3>Burger</h3>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
    </Carousel.Caption>
  </Carousel.Item>
  <Carousel.Item>
    <img
      className="d-block w-100"
      src={fried_chicken}
      alt="FriedChicken"
    />

    <Carousel.Caption>
      <h3>Fried Chicken</h3>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
    </Carousel.Caption>
  </Carousel.Item>

</Carousel>

);
}
export default Slider;