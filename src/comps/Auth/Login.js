import "./styles.scss";
import { Button, Carousel, Col, Form, Row } from "react-bootstrap";
import { Google } from "react-bootstrap-icons";
import { Link } from "react-router-dom";
import { useHistory } from "react-router";
import Slider from "./Carousel";
import { simpleNotification } from "../../utils/Notification";
import { useDispatch, useSelector } from "react-redux";
import { loginRequest } from "../../redux/actions";

const Login = () => {
const history = useHistory();
const dispatch = useDispatch();

const isLoggedIn  = useSelector(state => state.LoginReducer.isLoggedIn);
const credentials = {
  email: "amani@email.com",
  password: "password"
};
    const handleLogin = (e) => {
      e.preventDefault();
      const {email,password}  = e.target;
    if(password.value !== credentials.password || email.value !== credentials.email){
     simpleNotification("Incorrect Credentials","ERROR");
    }else{
         dispatch(loginRequest());
         history.push("/food");
    }
 

    }
return (
<div className="auth-container">
    <br/>
    <Row className="auth-row p-4">
    <Col className="auth-col-1 pt-5">
    <div className="login">
        <h1 className="login-tit">Login</h1>
        <p className="login-slo">Buy from us and get unlimmited support</p>
    {/* <div className="google-signin p-2 text-center">
       <Google/> Sign in with Google
    </div>
    <br/>
    <h1 className="text-center or">Or</h1>
    <br/> */}
    <Form className="login-form" onSubmit={handleLogin}>
  <Form.Group className="mb-3" controlId="formBasicEmail">
    <Form.Label>Email*</Form.Label>
    <Form.Control type="email" name="email" className="form-input" placeholder="Enter email" required />
  </Form.Group>

  <Form.Group className="mb-3" controlId="formBasicPassword">
    <Form.Label>Password*</Form.Label>
    <Form.Control type="password" name="password" className="form-input" placeholder="Password" required/>
  </Form.Group>
  <Row><Col><Form.Group className="mb-3" controlId="formBasicCheckbox">
    <Form.Check type="checkbox" label="Remember me" /> 
  </Form.Group></Col><Col><Link to="/" className="text-decoration-none">Forgot password?</Link></Col></Row>
  
  <Button variant="primary" className="login-btn" type="submit">
    Login
  </Button>
  <br/>
  <Row><Col>Not regitered yet? </Col><Col><Link to="/signup" className="text-decoration-none">Create account</Link></Col></Row>
</Form>
    </div>
    </Col>
    <Col className="auth-col-2 pt-5">
    <Slider/>
    </Col>
    </Row>
    </div>
)
};

export default Login;