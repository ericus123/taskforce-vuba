import { types } from "../actions/types";

const initialState = {
isLoggedIn: false,
};

const LoginReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.LOGGED_IN:
      return {
        ...state,
        isLoggedIn: action.payload
      };
      
    default:    
      return state;
  }
};

export default LoginReducer;