import { types } from "../actions/types";

const initialState = {
cart:[],
};

const CartReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.ITEM_ADDED:
      return {
        ...state,
        cart: [...state.cart,action.payload]
      };
      
    default:    
      return state;
  }
};

export default CartReducer;