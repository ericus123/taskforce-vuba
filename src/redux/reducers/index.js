import { combineReducers } from "redux";
import CartReducer from "./cart";
import LoginReducer from "./login";

// function that contains all reducer objects.
const allReducers = combineReducers({
    CartReducer,
    LoginReducer
});

export default allReducers;