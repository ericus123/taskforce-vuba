import { types } from "./types";

export const cartRequest = (item) => async (dispatch) => {
  try {
    dispatch({ type: types.ITEM_ADDED, payload: item });
  } catch (error) {
    console.log(error);
  }
};
export const loginRequest = () => async (dispatch) => {
  try {
    dispatch({ type: types.LOGGED_IN, payload: true });
  } catch (error) {
    console.log(error);
  }
};