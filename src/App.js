import './App.scss';
import {BrowserRouter as Router, Switch,Route} from "react-router-dom";
import HomePage from './comps/Homepage';
import Navigation from './comps/Navbar';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-credit-cards/es/styles-compiled.css';
import Login from './comps/Auth/Login';
import Signup from './comps/Auth/Signup';
import AllFood from './comps/Food';
import FoodInCart from './comps/Food/InCart';


function App() {
  return (
    <div className="App">
 
   <Router>
     <Navigation/>
     <Switch>
       <Route exact path="/" component={HomePage}/>
       <Route exact path="/food/in-cart" component={FoodInCart}/>
       <Route exact path="/login" component={Login} />
       <Route exact path="/signup" component={Signup} />
       <Route exact path="/food" component={AllFood} />
     </Switch>
   </Router>
    </div>
  );
}

export default App;
