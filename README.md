
# FOODWEB

A coocked food selling website 

## Features
- Add food in cart
- SignIn
- SignUp
- Filter food
- Get food Details
## Tech Stack
`React`
`Redux`
`Sass`
`CSS3`
`Netlify`
`Yarn` 


  
## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/ericus123/taskforce-vuba
```

Go to the project directory

```bash
  cd taskforce-vuba
```

Install dependencies

```bash
  yarn install
```

Start the server

```bash
  yarn run start
```
## Deployment

This app is deployed on netlify

  
## Demo
[https://taskforce-vuba.netlify.app/](https://taskforce-vuba.netlify.app/)


  
## Screenshots

![App Screenshot](https://i2.paste.pics/b27da1b9d14cb2a28c51b6563f1b62cb.png)

  
## 🚀 About Me
A full stack web developer focused on crafting great web experiences.

  
## 🔗 Links
[![portfolio](https://img.shields.io/badge/my_portfolio-000?style=for-the-badge&logo=ko-fi&logoColor=white)](https://www.amanieric.com/)
[![linkedin](https://img.shields.io/badge/linkedin-0A66C2?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/amani-eric/)
[![twitter](https://img.shields.io/badge/twitter-1DA1F2?style=for-the-badge&logo=twitter&logoColor=white)](https://twitter.com/amaniericus)

  
## Feedback

If you have any feedback, please reach out to me at amaniericus@gmail.com

  